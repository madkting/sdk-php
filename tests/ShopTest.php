<?php

namespace Madkting;

date_default_timezone_set('America/Mexico_City');

/**
 * Description of ShopTest
 *
 * @author Jesus Camacho
 */
class ShopTest extends \PHPUnit_Framework_TestCase {
    
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->config = array('token'=>'22401a14e1c058f16159c50821e15dcaa2ac4c89');
    }

    public function testShopList() {
        $client = new MadktingClient($this->config);
        $shop = $client->serviceShop();
        
        $list = $shop->search();
        $this->assertTrue(is_array($list));
    }

}
