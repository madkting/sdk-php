<?php

namespace Madkting;

date_default_timezone_set('America/Mexico_City');

/**
 * Description of ShopTest
 *
 * @author Jesus Camacho
 */
class ShopTest extends \PHPUnit_Framework_TestCase {
    
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->config = array('token'=>'22401a14e1c058f16159c50821e15dcaa2ac4c89');
    }

    public function testClient() { 
        $client = new MadktingClient($this->config);
        $this->assertTrue(is_object($client));        
    }

    public function testService() {
        $client = new MadktingClient($this->config);
        $shop = $client->serviceShop();
        $this->assertTrue(is_object($shop));
    }

    public function testOrderStatus() {
        $list = MadktingClient::getOrderStatusDictionary();
        $this->assertTrue(is_array($list));
    }

    public function testFields() {
        $list = MadktingClient::getProductFieldsDictionary();
        $this->assertTrue(is_array($list));
    }

}
