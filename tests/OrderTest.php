<?php

namespace Madkting;

date_default_timezone_set('America/Mexico_City');

/**
 * Description of ShopTest
 *
 * @author Jesus Camacho
 */
class OrderTest extends \PHPUnit_Framework_TestCase {
    
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->config = array('token'=>'22401a14e1c058f16159c50821e15dcaa2ac4c89');
        $this->shop_pk = 82;
        $this->marketplace_pk = 13;
    }

    public function testOrderList() {
        $client = new MadktingClient($this->config);
        $order = $client->serviceOrder();
        $list = $order->search(array(
            'shop_pk' => $this->shop_pk,
            'marketplace_pk' => $this->marketplace_pk
        ));
        $this->assertTrue(is_array($list));
    }

}
