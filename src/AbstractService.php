<?php

namespace Madkting;

/**
 * Default AWS client implementation
 */
abstract class AbstractService {

    protected $uri;
    protected $credentials;
    protected $config;

    public function __construct($uri, Credentials\Credentials $credentials, $config) {
        $this->uri = $uri;
        $this->credentials = $credentials;
        $this->config = $config;
    }
    
    /**
     * 
     * @param string $endpoint [collection_default, individual_default, ...]
     * @return string
     */
    public function getEndpoint($endpoint='collection_default'){
        $uri = $this->config['endpoints'][$endpoint];
        return str_replace('{host}', $this->uri, $uri);
    }

    abstract function search($params=null);

    abstract function get($params);

    abstract function post($params);

    abstract function put($params);
    
    abstract function delete($params);
}
