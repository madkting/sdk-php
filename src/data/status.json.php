<?php

return array(
    'order' => array(
        'payment_required' => 'Pending payment',
        'paid' => 'Paid',
        'pending' => 'Pending shipping',
        'shipped' => 'Shipped',
        'delivered' => 'Delivered',
        'canceled' => 'Canceled',
        'returned' => 'Returned',
        'refunded' => 'Refund',
        'failed_delivery' => 'Failed delivered'
    )
);
