<?php

namespace Madkting\Product;

use Madkting\AbstractService;
use Madkting\Request;

class ProductService extends AbstractService {

    public function search($params=null) {
        $request = new Request($this->credentials);
        $uri = $this->getEndpoint('collection_default');
        $response = $request->get($uri, $params); 
        return $response->body;
    }

    public function get($params) {
        $request = new Request($this->credentials);
        $uri = $this->getEndpoint('individual_default');
        if( !isset($params['shop_pk']) ){
            throw new \InvalidArgumentException('shop_pk key not found');
        }
        if( !isset($params['product_pk']) ){
            throw new \InvalidArgumentException('product_pk key not found');
        }
        $response = $request->get($uri, $params);
        return $response->body;
    }
    
    /**
     * Create products
     * @param int $shop_pk
     * @param list $products
     * @return string - url feed location 
     */
    public function post($params){
        if( !isset($params['shop_pk']) ){
            throw new \InvalidArgumentException('shop_pk key not found');
        }
        $request = new Request($this->credentials);
        $uri = $this->getEndpoint('collection_default');
        $response = $request->post($uri, array('shop_pk' => $params['shop_pk']), $params['products']);
        $location = $response->headers['Location'];
        return $location;
    }

    public function put($params){
        if( !isset($params['shop_pk']) ){
            throw new \InvalidArgumentException('shop_pk key not found');
        }
        $request = new Request($this->credentials);
        $uri = $this->getEndpoint('collection_default');
        $response = $request->put($uri, array('shop_pk' => $params['shop_pk']), $params['products']);
        $location = $response->headers['Location'];
        return $location;
    }
    
    /**
     * 
     * @param list $products
     *      [
     *          {"pk" => int},
     *          {"pk" => int}
     *      ]
     * @return string - url feed location 
     */
    public function delete($params){        
        if( !isset($params['shop_pk']) ){
            throw new \InvalidArgumentException('shop_pk key not found');
        }
        $request = new Request($this->credentials);
        $uri = $this->getEndpoint('collection_default');
        $response = $request->delete($uri,array('shop_pk' => $params['shop_pk']), $params['products']);
        $location = $response->headers['Location'];
        return $location;
    }
    
}
