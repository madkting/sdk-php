<?php

namespace Madkting;
use Httpful\Http;
use Httpful\Request as HttpRequest;

/**
 * Default AWS client implementation
 */
class Request {
    
    private $credentials;
    private $headers = array('Accept'=>'application/json');
    private $followRedirects = true;

    public function __construct(Credentials\Credentials $credentials) {
        $this->credentials = $credentials;;
        $this->addHeaders(array('Authorization'=>$credentials->getSecurityToken()));
    }
    
    public function setFollowRedirects($bool){
        $this->followRedirects = (boolean)$bool;
    }
    
    private function _getRequest($type, $uri, $params=null, $body=null){
        $uri = $this->_replace_params_uri($uri, $params);
        $request = HttpRequest::init()
                ->method($type)
                ->uri($uri)
                ->addHeaders($this->headers)
                ->followRedirects($this->followRedirects);
        if( !empty($body) ){
            $request->body($body, 'application/json');
        }
        return $request;
    }
    
    private function _replace_params_uri($uri, $params){        
        $query = array();
        if( !empty($params) ){
            foreach ($params as $key=>$value){
                $uri_old = $uri;
                $uri = str_replace('{'.$key.'}', $value, $uri);
                if($uri_old==$uri){
                    $query[$key] = $value;
                }
            }
        }
        if( !empty($query) ){
            if( strpos($uri, '?') === False ){
                $uri  .= '?'. http_build_query($query);
            }else{
                $uri  .= '&'. http_build_query($query);
            }
        }
        return $uri;
    }

    public function addHeaders(array $headers){
        $this->headers  = array_merge($this->headers, $headers);
    }
    
    private function _check_error($response, $request=Null){        
        if($response->hasErrors()){
            if($response->hasBody()){
                if(is_object($response->body)){                    
                    throw new Exception\MadktingException(
                        'You have validations errors, show ´body´ of response for more details',
                        array(
                            'request'=>$request,
                            'response'=>$response,
                            'result'=>$response->body
                        )
                    );
                }elseif(is_array($response->body)){
                    /*$message = '';                
                    foreach($response->body as $error){
                        foreach($error as $key=>$messages){
                            if(is_array($messages) && !empty($message)){
                                if(is_object($message[0])){
                                    $message .= PHP_EOL.$key.': '. print_r($messages, true);
                                }else{
                                    $message .= PHP_EOL.$key.': '. implode(PHP_EOL, $messages);
                                }
                            }else if(is_object($messages)){
                                $values = '';
                                foreach($messages as $messages2){
                                    if(is_array($messages2) && !empty($messages2)){
                                        if(is_object($messages2[0])){
                                            $values .= PHP_EOL.print_r($messages2, true);
                                        }else{
                                            $values .= implode(PHP_EOL,$messages2);
                                        }
                                        $values .= implode(PHP_EOL,$messages2);
                                    }else{
                                        $values .= PHP_EOL.$messages2;
                                    }
                                }
                                $message = PHP_EOL.$key.': '.$values;
                            }elseif(is_string($messages)) {
                                $message = PHP_EOL.$key.': '.$messages;
                            }
                        }                        
                    }*/
                    throw new Exception\MadktingException(
                        'You have validations errors, show ´body´ of response for more details',
                        array(
                            'request'=>$request,
                            'response'=>$response,
                            'result'=>$response->body
                        )
                    );
                }else{
                    throw new Exception\MadktingException(
                        $response->body,
                        array(
                            'request'=>$request,
                            'response'=>$response,
                            'result'=>$response->body
                        )
                    );
                }
            }else{
                throw new \Exception('Unknown error');
            }
        }
    }
    
    /**
     * GET Verb
     * @param string $uri 
     * @param array $params
     * @return \Httpful\Response
     */
    public function get($uri, array $params=null){
        $request = $this->_getRequest(Http::GET, $uri, $params);
        $response = $request->sendIt();
        $this->_check_error($response, $request);
        return $response;
    }
    
    /**
     * POST Verb
     * @param string $uri 
     * @param array $params
     * @param array $data
     * @return \Httpful\Response
     */
    public function post($uri, array $params=null, $data=null){
        $request = $this->_getRequest(Http::POST, $uri, $params, $data); 
        $response = $request->sendIt();
        #var_dump($response);
        $this->_check_error($response, $request);
        return $response;
    }
    
    
    /**
     * PUT Verb
     * @param string $uri 
     * @param array $params
     * @param array $data
     * @return \Httpful\Response
     */
    public function put($uri, array $params=null, $data=null){
        $request = $this->_getRequest(Http::PUT, $uri, $params, $data);
        $response = $request->sendIt();
        $this->_check_error($response, $request);
        return $response;
    }
       
    
    /**
     * DELETE Verb
     * @param string $uri 
     * @param array $params
     * @param array $data
     * @return \Httpful\Response
     */
    public function delete($uri, array $params=null, $data=null){
        $request = $this->_getRequest(Http::DELETE, $uri, $params, $data);
        $response = $request->sendIt();
        $this->_check_error($response, $request);
        return $response;
    }
}
